# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

import re
import sys
import unicodedata
from lxml.etree import _Element


def textify_node(node):
    if isinstance(node, _Element):
        return node.xpath('.//text()')[0]
    else:
        return node


def concat(context, items):
    items = [textify_node(i) for i in items]
    return ''.join(items)


def ends_with(context, item, sub):
    if type(item) is list:
        for i in item:
            return i.endswith(sub)


def lower_case(context, item, *args, **kwargs):
    if type(item) is list:
        items = []
        for i in item:
            items.append(i.lower())
        return items
    return item


def normalize_space(context, item):
    if type(item) is list:
        for i in item:
            return re.sub('\s+', ' ', i).strip()


def normalize_unicode(context, item, form='NFC'):
    if type(item) is list:
        for i in item:
            if form in ['NFC', 'NFKC', 'NFD', 'NFKD']:
                return unicodedata.normalize(form, i)
            raise Exception(f'{form} is not a valid normalization form.')


def starts_with(context, item, sub):
    if type(item) is list:
        for i in item:
            return i.startswith(sub)


def string_join(context, items, separator):
    items = [textify_node(i) for i in items]
    return separator.join(items)


def string_length(context, item):
    if type(item) is list:
        for i in item:
            return len(i)


def substring(context, item, starting_loc, length=sys.maxsize):
    if type(item) is list:
        for i in item:
            # imitate spec logic and disable negative slice indexing
            start = int(round(starting_loc)) - 1
            end = int(round(length))

            return i[max(0, start):max(0, start+end):]


def upper_case(context, item, *args, **kwargs):
    if type(item) is list:
        items = []
        for i in item:
            items.append(i.upper())
        return items
    return item


# -----------------------------------------------------------------------------
# EXPORTS
# -----------------------------------------------------------------------------

ALL_FUNCTIONS = {
    'concat': concat,
    'ends-with': ends_with,
    'lower-case': lower_case,
    'normalize-space': normalize_space,
    'normalize-unicode': normalize_unicode,
    'starts-with': starts_with,
    'string-join': string_join,
    'string-length': string_length,
    'substring': substring,
    'upper-case': upper_case,
}
