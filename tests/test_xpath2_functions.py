# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

import sys

from lxml import etree
from lxml.etree import XPathEvalError, fromstring as str2xml
from unittest2 import TestCase

import xpath2_functions


class Xpath2Functions(TestCase):

    def test_functions_argument(self):
        xpath2_functions.register_functions(etree, functions=[])
        msg = str2xml('<msg><out><status>X</status></out><out><status>Y</status></out><out><status>Z</status></out></msg>')
        self.assertRaisesRegex(XPathEvalError, 'Unregistered function', msg.xpath, 'xp2f:string-join(//status/text(), ",")')

    def test_functions_empty_ns(self):
        xpath2_functions.register_functions(etree, ns=None)
        msg = str2xml('<msg><out><status>X</status></out><out><status>Y</status></out><out><status>Z</status></out></msg>')
        self.assertEqual(msg.xpath('string-join(//status/text(), ",")'), 'X,Y,Z')

    def test_concat(self):
        xpath2_functions.register_functions(etree)
        msg = str2xml('<msg><out><status>X</status></out><out><status>Y</status></out><out><status>Z</status></out></msg>')
        self.assertEqual(msg.xpath('xp2f:concat(//status/text())'), 'XYZ')

    def test_endswith(self):
        xpath2_functions.register_functions(etree)
        msg = str2xml('<msg><out><status>test_X</status></out></msg>')
        self.assertEqual(msg.xpath('xp2f:ends-with(//status/text(), "_X")'), True)

    def test_join_string(self):
        xpath2_functions.register_functions(etree)
        msg = str2xml('<msg><out><status>X</status></out><out><status>Y</status></out><out><status>Z</status></out></msg>')
        self.assertEqual(msg.xpath('xp2f:string-join(//status/text(), ",")'), 'X,Y,Z')
        self.assertEqual(msg.xpath('xp2f:string-join(//status, ",")'), 'X,Y,Z')
        self.assertEqual(msg.xpath('xp2f:string-join(//out, ",")'), 'X,Y,Z')
        self.assertEqual(msg.xpath('xp2f:string-join(//out2, ",")'), '')

        if sys.version_info < (3, 0):
            exception_text = 'string_join\(\) takes exactly 3 arguments \(2 given\)'
        else:
            exception_text = 'string_join\(\) missing 1 required positional argument: \'separator\''

        self.assertRaises(TypeError, exception_text, msg.xpath, 'xp2f:string-join(//out2)')

    def test_lowercase_string(self):
        xpath2_functions.register_functions(etree)
        msg = str2xml('<msg><out><status>X</status></out></msg>')
        self.assertEqual(msg.xpath('xp2f:lower-case(//status/text())'), ['x'])

    def test_normalize_space(self):
        xpath2_functions.register_functions(etree)
        msg = str2xml('<msg><out><status> This is  a test. </status></out></msg>')
        self.assertEqual(msg.xpath('xp2f:normalize-space(//status/text())'), 'This is a test.')

    def test_normalize_unicode(self):
        xpath2_functions.register_functions(etree)
        msg = str2xml('<msg><out><status>I plan to go to Mu&#x0308;nchen in September.</status></out></msg>')
        self.assertEqual(msg.xpath('xp2f:normalize-unicode(//status/text())'), 'I plan to go to München in September.')

    def test_startswith(self):
        xpath2_functions.register_functions(etree)
        msg = str2xml('<msg><out><status>test_X</status></out></msg>')
        self.assertEqual(msg.xpath('xp2f:starts-with(//status/text(), "test_")'), True)

    def test_string_length(self):
        xpath2_functions.register_functions(etree)
        msg = str2xml('<msg><out><status>test_X</status></out></msg>')
        self.assertEqual(msg.xpath('xp2f:string-length(//status/text())'), 6)

    def test_substring(self):
        xpath2_functions.register_functions(etree)
        msg = str2xml('<msg><out><status>motor car</status></out></msg>')
        msg2 = str2xml('<msg><out><status>metadata</status></out></msg>')
        msg3 = str2xml('<msg><out><status>12345</status></out></msg>')
        self.assertEqual(msg.xpath('xp2f:substring(//status/text(), 6)'), ' car')
        self.assertEqual(msg2.xpath('xp2f:substring(//status/text(), 4, 3)'), 'ada')
        self.assertEqual(msg3.xpath('xp2f:substring(//status/text(), 1.5, 2.6)'), '234')
        self.assertEqual(msg3.xpath('xp2f:substring(//status/text(), 0, 3)'), '12')
        self.assertEqual(msg3.xpath('xp2f:substring(//status/text(), 5, -3)'), '')
        self.assertEqual(msg3.xpath('xp2f:substring(//status/text(), -3, 5)'), '1')
